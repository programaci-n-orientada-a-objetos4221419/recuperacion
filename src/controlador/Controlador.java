
package controlador;

import vista.dlgVista;
import modelo.ReciboLuz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * author Estrella
 */
public class Controlador implements ActionListener {
    private ReciboLuz recibo;
    private dlgVista vista;

    public Controlador(ReciboLuz recibo, dlgVista vista) {
        this.recibo = recibo;
        this.vista = vista;

        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);

        vista.boxTipoServicio.addActionListener(this);
    }
    private void iniciarVista() {
        vista.setTitle(":: Recibo de Luz ::");
        vista.setSize(600, 600);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            vista.txtNumRecibo.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomi.setEnabled(true);
            vista.boxTipoServicio.setEnabled(true);
            vista.txtKilConsu.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            limpiarCampos();
        } 
        else if (e.getSource() == vista.btnGuardar) {
            if (camposLlenos()) {
                try {
                    int numRecibo = Integer.parseInt(vista.txtNumRecibo.getText());
                    String fecha = vista.txtFecha.getText();
                    String nombre = vista.txtNombre.getText();
                    String domicilio = vista.txtDomi.getText();
                    int tipoServicio = vista.boxTipoServicio.getSelectedIndex() + 1;
                    float costoKilowatts = Float.parseFloat(vista.txtCostosKil.getText());
                    int kilowattsConsumidos = Integer.parseInt(vista.txtKilConsu.getText());

                    recibo.setNumRecibo(numRecibo);
                    recibo.setFecha(fecha);
                    recibo.setNombre(nombre);
                    recibo.setDomicilio(domicilio);
                    recibo.setTipoServicio(tipoServicio);
                    recibo.setCostokilowatts(costoKilowatts);
                    recibo.setKilowatts(kilowattsConsumidos);

                    JOptionPane.showMessageDialog(vista, "Registro Exitoso.");
                     vista.btnMostrar.setEnabled(true);
                    limpiarCampos();
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "Ocurrió un error al guardar el registro.\nPor favor, verifique los valores ingresados.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(vista, "Por favor, complete todos los campos.", "Campos incompletos", JOptionPane.WARNING_MESSAGE);
            }
        }
        else if (e.getSource() == vista.btnMostrar) {
            vista.txtNumRecibo.setText(String.valueOf(recibo.getNumRecibo()));
            vista.txtFecha.setText(recibo.getFecha());
            vista.txtNombre.setText(recibo.getNombre());
            vista.txtDomi.setText(recibo.getDomicilio());
            vista.txtCostosKil.setText(String.valueOf(recibo.getCostokilowatts()));
            vista.txtKilConsu.setText(String.valueOf(recibo.getKilowatts()));
            vista.boxTipoServicio.setSelectedIndex(recibo.getTipoServicio() - 1);

            double subtotal = recibo.calcularSubtotal();
            double impuestos = recibo.calcularImpuestos();
            double totalPagar = recibo.calcularTotalPagar();

            vista.txtSubtotal.setText(String.valueOf(subtotal));
            vista.txtImpuestos.setText(String.valueOf(impuestos));
            vista.txtTotal.setText(String.valueOf(totalPagar));
            deshabilitarCampos();     
        }  
        else if (e.getSource() == vista.boxTipoServicio) {
            int tipoServicio = vista.boxTipoServicio.getSelectedIndex();
            recibo.actualizarPrecioKilowatts(tipoServicio);
            float costoKilowatts = recibo.getCostokilowatts();
            vista.txtCostosKil.setText(String.valueOf(costoKilowatts));
            vista.txtCostosKil.setEnabled(tipoServicio != 2);
        }
        else if (e.getSource() == vista.btnLimpiar) {
            limpiarCampos();
        }
        else if (e.getSource() == vista.btnCancelar) {
            deshabilitarCampos();
        }   
        else if (e.getSource() == vista.btnCerrar) {
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Desea cerrar la aplicación?", "Cerrar", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                System.exit(0);
            }
        } 
    }
    private void limpiarCampos() {
        vista.txtNumRecibo.setText("");
        vista.txtFecha.setText("");
        vista.txtNombre.setText("");
        vista.txtDomi.setText("");
        vista.boxTipoServicio.setSelectedIndex(0);
        vista.txtCostosKil.setText("");
        vista.txtKilConsu.setText("");
        vista.txtSubtotal.setText("");
        vista.txtImpuestos.setText("");
        vista.txtTotal.setText("");
    }

    private void deshabilitarCampos() {
        vista.txtNumRecibo.setEnabled(false);
        vista.txtFecha.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtDomi.setEnabled(false);
        vista.boxTipoServicio.setEnabled(false);
        vista.txtCostosKil.setEnabled(false);
        vista.txtKilConsu.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
    }
    
    private boolean camposLlenos() {
        String numRecibo = vista.txtNumRecibo.getText();
        String fecha = vista.txtFecha.getText();
        String nombre = vista.txtNombre.getText();
        String domicilio = vista.txtDomi.getText();
        String costoKilowatts = vista.txtCostosKil.getText();
        String kilowattsConsumidos = vista.txtKilConsu.getText();

        return !numRecibo.isEmpty() && !fecha.isEmpty() && !nombre.isEmpty() && !domicilio.isEmpty()
                && !costoKilowatts.isEmpty() && !kilowattsConsumidos.isEmpty();
    } 
   public static void main(String[] args) {
        ReciboLuz recibo = new ReciboLuz();
        dlgVista vista = new dlgVista();
        Controlador controlador = new Controlador(recibo, vista);
        controlador.iniciarVista();
    }
}

